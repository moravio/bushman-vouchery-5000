<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Losování vítězů</title>
    <style>
        html {
            height: 100%;
        }

        body {
            margin: 25px;
            /*border: 1px solid black;*/
            background: url(img/center-bg.jpg) no-repeat transparent;
            height: calc(100% - 50px);
            background-size: cover;
        }

        form {
            height: 100%;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
        }

         img {
             position: absolute;
             left: 50px;
             top: 50px;
         }

        p#error {
            display: none;
            color: red;
        }
    </style>
</head>
<body>
    <img src="img/bushman-logo.png" />
    <form id="form" action="form-result.php" method="POST" enctype="multipart/form-data">
        <p id="error"></p>
        <input type="file" id="inputFile" name="inputFile" />
<!--        <input type="submit" value="Upload Image" name="submit">-->
    </form>
    <script>
        document.getElementById("inputFile").onchange = function() {
            var filename = document.getElementById("inputFile").value;
            var extension = filename.split('.').pop();
            if(extension === "csv" || extension === "xlsx")
                document.getElementById("form").submit();
            else {
                document.getElementById("error").innerText = "Soubor musí být .xlsx nebo .csv";
                document.getElementById("error").style.display = "block";
            }
        };
    </script>
</body>
</html>
